package com.zuitt.example;

//inherit from parent class
// "extend" keyword is used to inherit the properties  and methods of the parent class.
//a class cannot extend multiple classes, Driver
public class Dog extends Animal {
    // create properties
    private String breed;

    //empty constructor
    public Dog(){
        //super(); to inherit from parent class
        super();
        //default breed value
        this.breed = "Chihuahua";
    }

    //we now have access to the animal class
    public Dog(String name, String color, String breed){
        //if empty super(), refers to the empty animal constructor
        //if super() has params, refer to params constructor

        super(name, color);
        this.breed = breed;
    }

    //setter and getter
    public String getBreed(){
        return this.breed;
    }
    public void setBreed(String breed){
        this.breed = breed;
    }

    //method
    public void speak(){
        System.out.println("Woof woof!");
    }

    public void call(){
        //direct access with the parent method
        //super.call();
        System.out.println("Hi my name is " + this.name + ", I am a dog" );
    }

}
