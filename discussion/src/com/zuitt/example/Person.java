package com.zuitt.example;

//interface implementation
//use implements keyword
//can use multiple implementations
public class Person implements Actions, Greetings {
    //we must implement the Actions
    public void sleep(){
        System.out.println("ZZZ...");
    }
    public void run(){
        System.out.println("Running.");
    }

    public void morningGreet(){
        System.out.println("Good morning!");
    }
    public void holidayGreet() {
        System.out.println("Happy Holidays!");
    }

}
