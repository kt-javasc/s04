package com.zuitt.example;

//Serves as the Parent Class
public class Animal {
    //we will add properties
    protected String name;
    protected String color;

    //constructor

   //empty constructor
   public Animal(){}

   //parameterized constructor
    //why do we not use protected? they are in the same package
    //why do we apply protected? they are in the diff package
   public Animal(String name, String color){
       this.name = name;
       this.color = color;
   }

   //setter and getter

    //getter
    public String getName(){
       return this.name;
    }
    public String getColor(){
       return this.color;
    }

    //setter
    public void setName(String name){
       this.name = name;
    }
    public void setColor(String color){
        this.color = color;
    }

    //add methods

    public void call(){
       System.out.println("Hi, my name is " + this.name + "!");
    }




}
